"""
WSGI config for comments_proj project.
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "comments_proj.settings")

application = get_wsgi_application()
