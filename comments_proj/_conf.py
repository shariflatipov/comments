DEBUG = True

ALLOWED_HOSTS = []


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'comments',
        'HOST': 'localhost',
        'PORT': '5432',
        'PASSWORD': '123',
        'USER': 'postgres'
    }
}
