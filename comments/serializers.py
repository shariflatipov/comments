"""Comments serializer module"""
from rest_framework import serializers

from comments.models import Comment


class CommentSerializer(serializers.ModelSerializer):
    """Comment serializer"""

    class Meta:
        model = Comment
        fields = '__all__'
